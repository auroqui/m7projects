/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m07.DAO;

import m07.entitats.Cerca;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import m07.entitats.Film;

/**
 *
 * @author rvallez
 */
public class FilmDAO {
    private EntityManagerFactory emf = null;

    public FilmDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public List<Film> findFilm() {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createNamedQuery("Film.findAll", Film.class);
            return q.getResultList();

        } finally {
            em.close();
        }
    }
    
    public Film findById(int filmId) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createNamedQuery("Film.findById", Film.class);
            q.setParameter("filmId", filmId);
            return ((Film) q.getSingleResult());

        } finally {
            em.close();
        }
    }
    
    public Film findFilm(String title) {
        EntityManager em = getEntityManager();

        try {
            Query query = em.createNamedQuery("Film.findByTitle", Film.class
            );
            query.setParameter("title", title);
            return (Film) query.getSingleResult();

        } finally {
            em.close();
        }
    }
    
        public List<Cerca> perRating() {
        EntityManager em = getEntityManager();
        List<Cerca> ls = new ArrayList<>();
        try {
            Query query = em.createNamedQuery("Film.rating", Object[].class);

            List<Object[]> results = query.getResultList();

            for (Object[] result : results) {
                Cerca sc = new Cerca(Long.parseLong(result[0].toString()), result[1].toString());
                ls.add(sc);
            }

        } finally {
            em.close();
        }
        return ls;
    }
        
            public List<Cerca> perDuracio() {
        EntityManager em = getEntityManager();
        List<Cerca> ls = new ArrayList<>();
        try {
            Query query = em.createNamedQuery("Film.rental_duration", Object[].class);

            List<Object[]> results = query.getResultList();

            for (Object[] result : results) {
                Cerca sc = new Cerca (Long.parseLong(result[0].toString()), result[1].toString());
                ls.add(sc);
            }

        } finally {
            em.close();
        }
        return ls;
    }

    public List<Cerca> perPreu() {
        EntityManager em = getEntityManager();
        List<Cerca> ls = new ArrayList<>();
        try {
            Query query = em.createNamedQuery("Film.rental_rate", Object[].class);

            List<Object[]> results = query.getResultList();

            for (Object[] result : results) {
                Cerca sc = new Cerca(Long.parseLong(result[0].toString()), result[1].toString());
                ls.add(sc);
            }

        } finally {
            em.close();
        }
        return ls;

    }

        
}
