/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m07.charts.test;

import java.net.URL;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import javax.persistence.EntityManagerFactory;
import m07.entitats.Cerca;
import m07.DAO.DAO;
import m07.DAO.FilmDAO;

/**
 *
 * @author rvallez
 */
public class FXMLDocumentController implements Initializable {

    Scene scene;
    Stage stage = new Stage();
    List<Cerca> cercaEmmagatzemada = new ArrayList<>();
    String ordre, tipus;

    @FXML
    private void buscarAction(ActionEvent event) throws ParseException {
        EntityManagerFactory emf = DAO.getEntityManagerFactory();
        FilmDAO filmDao = new FilmDAO(emf);
        if (ordre.equals("rating")) {

            cercaEmmagatzemada = filmDao.perRating();
            System.out.println(cercaEmmagatzemada);
        } else if (ordre.equals("preu")) {

            cercaEmmagatzemada = filmDao.perPreu();
            System.out.println(cercaEmmagatzemada);

        } else {
            cercaEmmagatzemada = filmDao.perDuracio();
            System.out.println(cercaEmmagatzemada);
        }

        if (tipus.equals("linear")) {
            lineal(cercaEmmagatzemada);
        } else if (tipus.equals("circular")) {
            cercle(cercaEmmagatzemada);
        } else {
            barres(cercaEmmagatzemada);
        }

        stage.setScene(scene);
        stage.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void lineal(List<Cerca> dataRetrieved) {
        final NumberAxis X = new NumberAxis();
        final NumberAxis Y = new NumberAxis();

        final LineChart<Number, Number> lineChart
                = new LineChart<>(X, Y);
        XYChart.Series series = new XYChart.Series();

        Y.setLabel("dies");

        X.setLabel("preu");

        lineChart.setTitle("Pelicules");
        //series.setName(ordre);

        dataRetrieved.forEach((film) -> {
            Double n = Double.valueOf(film.getValor());
            int s = (int) film.getCont();
            series.getData().add(new XYChart.Data<>(s, n));
        });

        lineChart.getData().add(series);
        scene = new Scene(lineChart, 600, 600);

    }

    public void cercle(List<Cerca> dataRetrieved) {
        stage.setTitle("pelicules Summary");
        scene = new Scene(new Group());

        ObservableList<PieChart.Data> pieChartData
                = FXCollections.observableArrayList();
        for (Cerca film : dataRetrieved) {
            Double n = Double.valueOf(film.getValor());
            String s = String.valueOf(film.getCont());
            pieChartData.add(new PieChart.Data(s, n));
        }
        final PieChart chart = new PieChart(pieChartData);
        ((Group) scene.getRoot()).getChildren().add(chart);
    }

    public void barres(List<Cerca> dataRetrieved) throws ParseException {
        final CategoryAxis x = new CategoryAxis();
        final NumberAxis y = new NumberAxis();
        final BarChart<String, Number> bc
                = new BarChart<>(x, y);
        XYChart.Series series = new XYChart.Series();
        y.setLabel("dies");
        if (this.ordre.equals("preu")) {
            x.setLabel("preu");
        } else {
            x.setLabel("pelicula");
        }

        bc.setTitle("pelicules");

        //series.setName("");
        for (Cerca film : dataRetrieved) {
            Double n = Double.valueOf(film.getValor());
            String str = (String) film.getValor();
            series.getData().add(new XYChart.Data(str, n));

        }
        bc.getData().add(series);
        scene = new Scene(bc, 800, 600);
    }

    @FXML
    private void linearHandler(ActionEvent event) {
        this.tipus = "linear";

    }

    @FXML
    private void circularHandler(ActionEvent event) {
        this.tipus = "circular";
    }

    @FXML
    private void barresHandler(ActionEvent event) {
        this.tipus = "barres";
    }

    @FXML
    private void valoracioHandler(ActionEvent event) {
        this.ordre = "rating";
    }

    @FXML
    private void preuHandler(ActionEvent event) {
        this.ordre = "preu";
    }

    @FXML
    private void duracioHandler(ActionEvent event) {
        this.ordre = "duracio";
    }

}
