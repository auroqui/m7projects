/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m07.entitats;

/**
 *
 * @author auroq
 */
public class Cerca {

    long cont;
    String valor;

    public Cerca(long cont, String valor) {
        this.cont = cont;
        this.valor = valor;
    }

    public long getCont() {
        return cont;
    }

    public void setCont(long cont) {
        this.cont = cont;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Cerca{" + "cont=" + cont + ", valor=" + valor + '}';
    }



  

}
