package m07.entitats;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Store.class)
public abstract class Store_ {

	public static volatile ListAttribute<Store, Film> films;
	public static volatile SingularAttribute<Store, Timestamp> lastUpdate;
	public static volatile SingularAttribute<Store, Integer> storeId;
	public static volatile SingularAttribute<Store, Integer> managerStaffId;
	public static volatile SingularAttribute<Store, Integer> addressId;

}

