package m07.entitats;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Film.class)
public abstract class Film_ {

	public static volatile SingularAttribute<Film, String> Description;
	public static volatile SingularAttribute<Film, Float> rentalRate;
	public static volatile SingularAttribute<Film, Integer> rentalDuration;
	public static volatile ListAttribute<Film, Store> stores;
	public static volatile SingularAttribute<Film, Integer> languageId;
	public static volatile SingularAttribute<Film, Integer> length;
	public static volatile SingularAttribute<Film, String> rating;
	public static volatile SingularAttribute<Film, Float> replacementCost;
	public static volatile SingularAttribute<Film, String> title;
	public static volatile SingularAttribute<Film, String> specialFeatures;
	public static volatile SingularAttribute<Film, Integer> filmId;
	public static volatile SingularAttribute<Film, Timestamp> lastUpdate;
	public static volatile SingularAttribute<Film, Integer> originalLanguageId;
	public static volatile SetAttribute<Film, Category> categories;
	public static volatile SingularAttribute<Film, String> releaseYear;

}

