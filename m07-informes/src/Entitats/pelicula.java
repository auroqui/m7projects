/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entitats;

/**
 *
 * @author auroq
 */
public class pelicula {

    int film_id;
    String title;
    String release_year;
    String rating;
    int original_language_id;

    public pelicula(int film_id, String title, String release_year, String rating, int original_language_id) {
        this.film_id = film_id;
        this.title = title;
        this.release_year = release_year;
        this.rating = rating;
        this.original_language_id = original_language_id;
    }

    public int getFilm_id() {
        return film_id;
    }

    public void setFilm_id(int film_id) {
        this.film_id = film_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelease_year() {
        return release_year;
    }

    public void setRelease_year(String release_year) {
        this.release_year = release_year;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public int getOriginal_language_id() {
        return original_language_id;
    }

    public void setOriginal_language_id(int original_language_id) {
        this.original_language_id = original_language_id;
    }

    @Override
    public String toString() {
        return "pelicula{" + "film_id=" + film_id + ", title=" + title + ", release_year=" + release_year + ", rating=" + rating + ", original_language_id=" + original_language_id + '}';
    }




    


}
