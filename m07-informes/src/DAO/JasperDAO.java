/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import conn.Connexio;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author auroq
 */
public class JasperDAO {

    public void createReport(String ruta) {

        try {
            String sql = "SELECT film_id, title, release_year, rating, original_language_id FROM film LIMIT 100";

            JasperDesign jd = JRXmlLoader.load(ruta);

            JRDesignQuery peliculesQuery = new JRDesignQuery();
            peliculesQuery.setText(sql);

            jd.setQuery(peliculesQuery);

            JasperReport jr = JasperCompileManager.compileReport(jd);

            JasperPrint reportToPrint = JasperFillManager.fillReport(
                    jr,
                    null,
                    Connexio.conectar());

            JasperViewer jViewer = new JasperViewer(reportToPrint);
            jViewer.setVisible(true);

        } catch (JRException ex) {
            Logger.getLogger(JasperDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(JasperDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void exportToWORD(String ruta) throws ClassNotFoundException, JRException {
        String sql = "SELECT film_id, title, release_year, rating, original_language_id FROM film LIMIT 100";

        JasperDesign jd = JRXmlLoader.load(ruta);

        JRDesignQuery peliculesQuery = new JRDesignQuery();
        peliculesQuery.setText(sql);

        jd.setQuery(peliculesQuery);

        JasperReport jr = JasperCompileManager.compileReport(jd);

        JasperPrint reportToPrint = JasperFillManager.fillReport(
                jr,
                null,
                Connexio.conectar());

        Exporter exporter = new JRDocxExporter();
        exporter.setExporterInput(new SimpleExporterInput(reportToPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("./reports/informe.docx"));
        exporter.exportReport();
    }

    public void exportToPDF(String ruta) throws JRException, ClassNotFoundException {
        String sql = "SELECT film_id, title, release_year, rating, original_language_id FROM film LIMIT 100";

        JasperDesign jd = JRXmlLoader.load(ruta);

        JRDesignQuery peliculesQuery = new JRDesignQuery();
        peliculesQuery.setText(sql);

        jd.setQuery(peliculesQuery);

        JasperReport jr = JasperCompileManager.compileReport(jd);

        JasperPrint reportToPrint = JasperFillManager.fillReport(
                jr,
                null,
                Connexio.conectar());

        Exporter exporter = new JRPdfExporter();
        exporter.setExporterInput(new SimpleExporterInput(reportToPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("./reports/informe.pdf"));
        exporter.exportReport();
    }
}
