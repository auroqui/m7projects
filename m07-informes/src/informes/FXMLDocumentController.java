/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package informes;

import DAO.JasperDAO;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.JRException;

/**
 *
 * @author rvallez
 */
public class FXMLDocumentController implements Initializable {

    Scene scene;
    Stage stage = new Stage();
    JasperDAO jDAO = new JasperDAO();

    @FXML
    private Label label;

    @FXML
    private void handleExportWORD(ActionEvent event) throws ClassNotFoundException, JRException {

        label.setText("Exporting document to WORD...");
        jDAO.exportToWORD("./reports/M7MyReport.jrxml");
        label.setText("Exported sucessfully");
    }

    @FXML
    private void handleVerReport(ActionEvent event) {
        
        label.setText("Creating report to visualize");
        jDAO.createReport("./reports/M7MyReport.jrxml");
        

    }

    @FXML
    private void handleExportPDF(ActionEvent event) throws JRException, ClassNotFoundException {

        label.setText("Exporting document to WORD...");
        jDAO.exportToPDF("./reports/M7MyReport.jrxml");
        label.setText("Exported sucessfully");

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
